import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {AppComponent} from "./app.component";
import {NavbarComponent} from "../navbar/navbar.component";
import {SidebarComponent} from "../sidebar/sidebar.component";
import {ContentComponent} from "../content/content.component";
import TestService from "../service/TestService";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule
    ],
    declarations: [
        AppComponent,
        NavbarComponent,
        SidebarComponent,
        ContentComponent
    ],
    providers: [
        TestService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
