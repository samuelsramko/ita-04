import {Component} from "@angular/core";
import TestService from "../service/TestService";

@Component({
    selector: 'navbar-component',
    templateUrl: './navbar.component.html'
})
export class NavbarComponent {

    private envelopeHref = 'https://google.com';
    private user;

    private AA = [1, 2, 3, 4, 5];

    constructor(private testService: TestService) {
        console.log(testService.getTestData());
        this.user = {
            name: 'Samuel',
            surname: 'Šramko'
        };
    }

    private onEnvelopeClick(event:Event, a) {
        console.log(a);
    }
}
